DROP TABLE IF EXISTS withholding_schedule;

DROP TABLE IF EXISTS withholding_allowance;

DROP TABLE IF EXISTS payroll_period;

DROP TABLE IF EXISTS marital_status;

DROP TABLE IF EXISTS tax_year;

DROP TABLE IF EXISTS tax_bracket;

CREATE TABLE tax_bracket(
    tax_bracket_id INTEGER PRIMARY KEY,
    tax_bracket_rate DECIMAL NOT NULL
);

CREATE TABLE tax_year(
    tax_year_id serial PRIMARY KEY,
    tax_year_value INTEGER NOT NULL
);

CREATE TABLE payroll_period(
    payroll_period_id serial PRIMARY KEY,
    payroll_period_name varchar(50) NOT NULL
);

CREATE TABLE marital_status(
    marital_status_id serial PRIMARY KEY,
    marital_status_name varchar(100) NOT NULL
);

CREATE TABLE withholding_allowance(
    withholding_allowance_id serial PRIMARY KEY,
    tax_year_id INTEGER REFERENCES tax_year(tax_year_id) ON DELETE RESTRICT NOT NULL,
    payroll_period_id INTEGER REFERENCES payroll_period(payroll_period_id) ON DELETE RESTRICT NOT NULL,
    withholding_allowance_amount DECIMAL NOT NULL
);

CREATE TABLE withholding_schedule(
    withholding_scedule_id serial PRIMARY KEY,
    tax_year_id INTEGER REFERENCES tax_year(tax_year_id) ON DELETE RESTRICT NOT NULL,
    tax_bracket_id INTEGER REFERENCES tax_bracket(tax_bracket_id) ON DELETE RESTRICT NOT NULL,
    payroll_period_id INTEGER REFERENCES payroll_period(payroll_period_id) ON DELETE RESTRICT NOT NULL,
    marital_status_id INTEGER REFERENCES marital_status(marital_status_id) ON DELETE RESTRICT NOT NULL,
    schedule_floor INTEGER NOT NULL,
    base_tax_amount DECIMAL NOT NULL
);