INSERT INTO tax_year(tax_year_value)
VALUES 
    (2019);

INSERT INTO tax_bracket(tax_bracket_id, tax_bracket_rate) 
VALUES 
    (1, 0.00),
    (2, 0.10),
    (3, 0.12),
    (4, 0.22),
    (5, 0.24),
    (6, 0.32),
    (7, 0.35),
    (8, 0.37);

INSERT INTO marital_status(marital_status_name) 
VALUES 
    ('Single'),
    ('Married');

INSERT INTO payroll_period(payroll_period_name) 
VALUES 
    ('Weekly'),
    ('Bi-weekly'), 
    ('Semi-monthly'), 
    ('Monthly'),
    ('Quarterly'),
    ('Semi-annually'),
    ('Annually'),
    ('Daily/Misc');

INSERT INTO withholding_allowance(payroll_period_id, tax_year_id, withholding_allowance_amount)
VALUES
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        80.80
    ),
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        161.50
    ),
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        175.00
    ),
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        350.00
    ),
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'),
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1050.00
    ),
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'),
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2100.00
    ),
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4200.00
    ),
    (
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'),
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        16.20
    );


-- insert single schedules

INSERT INTO withholding_schedule(tax_year_id, tax_bracket_id, payroll_period_id, marital_status_id, schedule_floor, base_tax_amount)
VALUES
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        73,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        260,
        18.70
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        832,
        87.34
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        1692,
        276.54
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        3164,
        629.82
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        3998,
        896.70
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        9887,
        2957.85
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        146,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        519,
        37.30
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        1664,
        174.70
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        3385,
        553.32
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        6328,
        1259.64
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        7996,
        1793.40
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        19773,
        5915.35
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        158,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        563,
        40.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        1803,
        189.30
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        3667,
        599.38
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        6855,
        1364.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        7996,
        1793.40
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        19773,
        5915.35
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        317,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        1125,
        80.80
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        3606,
        378.52
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        7333,
        1198.46
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        13710,
        2728.94
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        17325,
        3885.74
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        42842,
        12816.69
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        950,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        3375,
        242.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        10819,
        1135.78
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        22000,
        1135.78
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        41131,
        8187.04
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        51975,
        11657.12
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        128525,
        38449.62
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        1900,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        6750,
        485.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        21638,
        2271.56
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        44000,
        7191.20
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        82263,
        16374.32
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        103950,
        23314.16
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        257050,
        76899
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        3800,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        13500,
        970.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        43275,
        4543.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        88000,
        14382.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        164525,
        32748.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        207900,
        46628.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        514100,
        153798.50
    ),
        (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        14.60,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        51.90,
        3.73
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        166.40,
        17.47
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        338.50,
        55.33
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        632.80,
        125.96
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        799.60,
        179.34
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Single'),
        1977.30,
        591.54
    );

-- insert married schedules

INSERT INTO withholding_schedule(tax_year_id, tax_bracket_id, payroll_period_id, marital_status_id, schedule_floor, base_tax_amount)
VALUES
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        227,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        600,
        37.30
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        1745,
        174.70
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        3465,
        553.10
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        6409,
        1259.66
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        8077,
        1793.42
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        12003,
        3167.52
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        454,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        1200,
        74.60
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        3490,
        349.40
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        6931,
        1106.42
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        12817,
        2519.06
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        16154,
        3586.90
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Bi-weekly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        24006,
        6335.10
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        492,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        1300,
        40.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        3781,
        189.30
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        7508,
        599.38
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        13885,
        1364.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        17500,
        1793.40
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        26006,
        5915.35
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        983,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        2600,
        161.70
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        7563,
        757.26
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        15017,
        2397.14
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        27771,
        5458.10
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        35000,
        7771.38
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Monthly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        52013,
        13725.93
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        2950,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        7800,
        485
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        22688,
        2271.56
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        45050,
        7171.20
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        83313,
        16374.32
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        105000,
        23314.16
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Quarterly'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        153038,
        41177.46
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        5900,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        15600,
        970
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        45375,
        4543.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        90100,
        14382.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        166625,
        32748.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        210000,
        46628.50
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Semi-annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        312075,
        82354.75
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        11800,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        31200,
        1940.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        90750,
        9086.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        180200,
        28765.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        333250,
        65497.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        420000,
        93257.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Annually'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        624150,
        164709.50
    ),
        (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        1,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        0,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        2,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        45.00,
        0.00
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        3,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        120,
        7.46
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        4,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        349.00,
        34.94
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        5,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        693.10,
        110.64
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        6,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        1281.70,
        251.90
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        7,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        1615.40,
        358.68
    ),
    (
        (SELECT tax_year_id FROM tax_year where tax_year_value = 2019),
        8,
        (SELECT payroll_period_id FROM payroll_period where payroll_period_name = 'Daily/Misc'), 
        (SELECT marital_status_id FROM marital_status where marital_status_name = 'Married'),
        2400.60,
        633.50
    );