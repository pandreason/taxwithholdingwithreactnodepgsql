import React, { Component } from 'react';
import './withholding-allowance-text-component.css';

class WithholdingAllowanceText extends Component {
    state = {
        withholdingAllowances: ['loading...'],
        selectedWithholdingAllowanceId: 'loading...'
    }

    constructor(props) {
        super(props);

        this.change = this.change.bind(this);
    }

    async componentDidMount() {
        const withholdingAllowances = await window.fetch(`/api/withholding-allowances`)
        .then(response =>
            response.json()
                .then(value => {
                    return value.withholdingAllowances.map(allowance => allowance);
                })
        );


        await this.setState({ 
            withholdingAllowances: withholdingAllowances,
            selectedWithholdingAllowanceId: withholdingAllowances[0].id
        });

        this.props.onSelectedChange(this.state.selectedWithholdingAllowanceId);
    };

    async change(event) {
        await this.setState({ selectedWithholdingAllowanceId: event.target.value });
        this.props.onSelectedChange(this.state.selectedWithholdingAllowanceId);
    }

    render() {
        return (
            <select onChange={this.change} value={this.state.selectedWithholdingAllowanceId}>
                {this.state.withholdingAllowances.map(function (withholdingAllowance, key) {
                    return <option key={key} id={'withholding-allowance-' + withholdingAllowance.id} value={withholdingAllowance.id}>{withholdingAllowance.withholdingAllowanceAmount}</option>;
                })}
            </select>
        )
    }
}

export default WithholdingAllowanceText