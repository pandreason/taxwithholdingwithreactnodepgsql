import React, { Component } from 'react';
import './tax-bracket-text-component.css';

class TaxBracketText extends Component {
    state = {
        taxBrackets: ['loading...'],
        selectedtaxBracketId: 'loading...'
    }

    constructor(props) {
        super(props);

        this.change = this.change.bind(this);
    }

    async componentDidMount() {
        const taxBrackets = await window.fetch(`/api/tax-brackets`)
            .then(response =>
                response.json()
                    .then(value => {
                        return value.taxBrackets.map(bracket => bracket);
                    })
            );

        await this.setState({ 
            taxBrackets: taxBrackets,
            selectedtaxBracketId: taxBrackets[0].id
        });

        this.props.onSelectedChange(this.state.selectedtaxBracketId);
    };

    async change(event) {
        await this.setState({ selectedtaxBracketId: event.target.value });
        this.props.onSelectedChange(this.state.selectedtaxBracketId);
    }

    render() {
        return (
            <select onChange={this.change} value={this.state.selectedtaxBracketId}>
                {this.state.taxBrackets.map(function (taxBracket, key) {
                    return <option key={key} id={'tax-bracket-' + taxBracket.id} value={taxBracket.id}>{taxBracket.rate}</option>;
                })}
            </select>
        )
    }
}

export default TaxBracketText