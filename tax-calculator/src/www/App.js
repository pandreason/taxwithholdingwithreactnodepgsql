import React, { Component } from 'react';
import MaritalStatusSelect from './select-components/marital-status-select-component/marital-status-select-component.js';
import PayrollPeriodSelect from './select-components/payroll-period-select-component/payroll-period-select-component.js';
import TaxYearSelect from './select-components/tax-year-select-component/tax-year-select-component.js';
import TaxBracketText from './text-components/tax-bracket-text-component/tax-bracket-text-component.js';
import WithholdingAllowanceText from './text-components/withholding-allowance-text-component/withholding-allowance-text-component.js'
import './App.css';

class App extends Component {
    state = {
        selectedMaritalStatusId: 'loading...',
        selectedPayrollPeriodId: 'loading...',
        selectedTaxYearId: 'loading...',
        selectedTaxBracketId: 'loading...',
        selectedWithholdingAllowanceId: 'loading...'
    }

    constructor(props) {
        super(props);

        this.maritalStatusChanged = this.maritalStatusChanged.bind(this);
        this.taxYearChanged = this.taxYearChanged.bind(this);
        this.payrollPeriodChanged = this.payrollPeriodChanged.bind(this);
        this.taxBracketChanged = this.taxBracketChanged.bind(this);
        this.withholdingAllowanceChanged = this.withholdingAllowanceChanged.bind(this);
    }

    async maritalStatusChanged(val) {
        await this.setState({selectedMaritalStatusId: val});
    }

    async taxYearChanged(val) {
        await this.setState({selectedTaxYearId: val});
    }

    async payrollPeriodChanged(val) {
        await this.setState({selectedPayrollPeriodId: val});
    }

    async taxBracketChanged(val){
        await this.setState({selectedTaxBracketId: val})
    }

    async withholdingAllowanceChanged(val){
        await this.setState({selectedWithholdingAllowanceId: val})
    }

    render() {
        return (
            <div className="App">
                <h5>Tax Year: <TaxYearSelect onSelectedChange={this.taxYearChanged}/></h5>
                <h5>Marital Status: <MaritalStatusSelect onSelectedChange={this.maritalStatusChanged}/></h5>
                <h5>Payroll Period: <PayrollPeriodSelect onSelectedChange={this.payrollPeriodChanged}/></h5>
                <h5>Tax Brackets: <TaxBracketText onSelectedChange={this.taxBracketChanged} /></h5>
                <h5>Withholding Allowance: <WithholdingAllowanceText onSelectedChange={this.withholdingAllowanceChanged} /></h5>
            </div>
        )
    }
}

export default App
