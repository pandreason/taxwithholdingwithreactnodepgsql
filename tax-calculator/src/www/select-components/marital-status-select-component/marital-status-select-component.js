import React, { Component } from 'react';
import './marital-status-select-component.css';

class MaritalStatusSelect extends Component {
    state = {
        maritalStatuses: ['loading...'],
        selectedMaritalStatusId: 'loading...'
    }

    constructor(props) {
        super(props);

        this.change = this.change.bind(this);
    }

    async componentDidMount() {
        const maritalStatuses = await window.fetch(`/api/marital-statuses`)
            .then(response =>
                response.json()
                    .then(value => {
                        return value.maritalStatuses.map(status => status);
                    })
            );

        await this.setState({ 
            maritalStatuses: maritalStatuses,
            selectedMaritalStatusId: maritalStatuses[0].id
        });

        this.props.onSelectedChange(this.state.selectedMaritalStatusId);
    };

    async change(event)  {
        await this.setState({ selectedMaritalStatusId: event.target.value });
        this.props.onSelectedChange(this.state.selectedMaritalStatusId);
    }

    render() {
        return (
            <select onChange={this.change} value={this.state.selectedMaritalStatusId}>
                {this.state.maritalStatuses.map(function (maritalStatus, key) {
                    return <option key={key} id={'marital-status-' + maritalStatus.id} value={maritalStatus.id}>{maritalStatus.name}</option>;
                })}
            </select>
        )
    }
}

export default MaritalStatusSelect