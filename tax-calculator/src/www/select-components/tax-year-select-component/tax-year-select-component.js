import React, { Component } from 'react';
import './tax-year-select-component.css';

class TaxYearSelect extends Component {
    state = {
        taxYears: ['loading...'],
        selectedtaxYearId: 'loading...'
    }

    constructor(props) {
        super(props);

        this.change = this.change.bind(this);
    }

    async componentDidMount() {
        const taxYears = await window.fetch(`/api/tax-years`)
            .then(response =>
                response.json()
                    .then(value => {
                        return value.taxYears.map(year => year);
                    })
            );

        await this.setState({ 
            taxYears: taxYears,
            selectedtaxYearId: taxYears[0].id
        });

        this.props.onSelectedChange(this.state.selectedtaxYearId);
    };

    async change(event) {
        await this.setState({ selectedtaxYearId: event.target.selectedtaxYearId });
        this.props.onSelectedChange(this.state.selectedtaxYearId);
    }

    render() {
        return (
            <select onChange={this.change} value={this.state.selectedtaxYearId}>
                {this.state.taxYears.map(function (taxYear, key) {
                    return <option key={key} id={'tax-year-' + taxYear.id} value={taxYear.id}>{taxYear.year}</option>;
                })}
            </select>
        )
    }
}

export default TaxYearSelect