import React, { Component } from 'react';
import './payroll-period-select-component.css';

class PayrollPeriodSelect extends Component {
    state = {
        payrollPeriods: ['loading...'],
        selectedPayrollPeriodId: 'loading...'
    }

    constructor(props) {
        super(props);

        this.change = this.change.bind(this);
    }

    async componentDidMount() {
        const payrollPeriods = await window.fetch(`/api/payroll-periods`)
            .then(response =>
                response.json()
                    .then(value => {
                        return value.payrollPeriods.map(period => period);
                    })
            );

        await this.setState({ 
            payrollPeriods: payrollPeriods,
            selectedPayrollPeriodId: payrollPeriods[0].id
        });

        this.props.onSelectedChange(this.state.selectedPayrollPeriodId);
    };

    async change(event) {
        await this.setState({ selectedPayrollPeriodId: event.target.value });
        this.props.onSelectedChange(this.state.selectedPayrollPeriodId);
    }

    render() {
        return (
            <select onChange={this.change} value={this.state.selectedPayrollPeriodId}>
                {this.state.payrollPeriods.map(function (payrollPeriod, key) {
                    return <option key={key} id={'payroll-period-' + payrollPeriod.id} value={payrollPeriod.id}>{payrollPeriod.name}</option>;
                })}
            </select>
        )
    }
}

export default PayrollPeriodSelect