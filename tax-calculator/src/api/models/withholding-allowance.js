class WithholdingAllowance {
    constructor(id, taxYearId, payrollPeriodId, withholdingAllowanceAmount) {
        this.id = id;
        this.taxYearId = taxYearId;
        this.payrollPeriodId = payrollPeriodId;
        this.withholdingAllowanceAmount = withholdingAllowanceAmount;
    }
}

module.exports = {
    WithholdingAllowance: WithholdingAllowance
};