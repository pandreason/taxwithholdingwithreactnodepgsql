class WithholdingSchedule {
    constructor(id, taxYearId, taxBracketId, payrollPeriodId, maritalStatusId, scheduleFloor, baseTaxAmount) {
        this.id = id;
        this.taxYearId = taxYearId;
        this.taxYearId = taxBracketId;
        this.payrollPeriodId = payrollPeriodId;
        this.maritalStatusId = maritalStatusId;
        this.scheduleFloor = scheduleFloor;
        this.baseTaxAmount = baseTaxAmount;
    }
}

module.exports = {
    WithholdingSchedule: WithholdingSchedule
};