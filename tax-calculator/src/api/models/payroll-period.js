class PayrollPeriod {
    constructor(id, name){
        this.id = id;
        this.name = name;
    }
}

module.exports = {
    PayrollPeriod: PayrollPeriod
};