class TaxYear {
    constructor(id, year) {
        this.id = id;
        this.year = year;
    }
}

module.exports = {
    TaxYear: TaxYear
};