class TaxBracket {
    constructor(id, rate) {
        this.id = id;
        this.rate = rate;
    }
}

module.exports = {
    TaxBracket: TaxBracket
};