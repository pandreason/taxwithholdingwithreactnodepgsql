const { Pool } = require('pg');

class pgDatabaseContext {

    constructor(injectedConnectionPool) {
        if (injectedConnectionPool !== undefined)
            this.connectionPool = injectedConnectionPool;
        else this.connectionPool = this.GetConnectionPool();
    }

    GetConnectionPool() {
        const pgConfig = {
            user: 'pandreaspa',
            host: 'localhost',
            database: 'taxwithholding',
            password: 'admin',
            port: 5432
        };

        var pool = new Pool(pgConfig);

        pool.on('error', (err, client) => {
            console.error('Unexpected error on idle client', err);
            process.exit(-1);
        })

        return pool;
    }
}

module.exports = {
    pgDatabaseContext: pgDatabaseContext,
}