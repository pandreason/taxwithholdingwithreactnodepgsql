const express = require('express');
require('dotenv').config();
const { taxDataService}  = require('./tax-data-service.js');
const { pgDatabaseContext } = require('./database-context.js')
const bodyParser = require('body-parser');

const app = express();
const port = 4000;
app.use(bodyParser.json());

const dataService = new taxDataService(new pgDatabaseContext())

app.get('/api/marital-statuses', (req, res) => {
    dataService.SelectMaritalStatuses()
    .then(resp => res.send({ maritalStatuses: resp }));
});

app.get('/api/payroll-periods', (req, res) => {
    dataService.SelectPayrollPeriods()
    .then(resp => res.send({ payrollPeriods: resp}));
});

app.get('/api/tax-brackets', (req, res) => {
    dataService.SelectTaxBrackets()
    .then(resp => res.send({ taxBrackets: resp}));
});

app.get('/api/tax-brackets/:id', (req, res) => {
    dataService.SelectTaxBracketById(req.params.id)
    .then(resp => res.send({ taxBrackets: resp}));
});

app.get('/api/tax-years', (req, res) => {
    dataService.SelectTaxYears()
    .then(resp => res.send({ taxYears: resp}));
});

app.get('/api/withholding-allowances', (req, res) => {
    dataService.SelectWithholdingAllowances()
    .then(resp => res.send({withholdingAllowances: resp}));
});

app.get('/api/withholding-allowances/:id', (req, res) => {
    dataService.SelectWithholdingAllowanceById(req.params.id)
    .then(resp => res.send({withholdingAllowances: resp}));
});

app.listen(port, () => console.log(`Example backend API listening on port ${port}!`))
