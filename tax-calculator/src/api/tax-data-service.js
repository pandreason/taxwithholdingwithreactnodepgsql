const { MaritalStatus } = require('./models/marital-status.js');
const { PayrollPeriod } = require('./models/payroll-period.js');
const { TaxBracket } = require('./models/tax-bracket.js');
const { TaxYear } = require('./models/tax-year.js');
const { WithholdingAllowance } = require('./models/withholding-allowance.js');
const { WithholdingSchedule } = require('./models/withholding-schedule.js');


class taxDataService {
    constructor(injectedDatabaseContext) {
        this.context = injectedDatabaseContext;
    }

    SelectMaritalStatuses() {
        var connectionPool = this.context.connectionPool;

        return connectionPool.connect()
            .then(client => {
                return client.query('SELECT * FROM marital_status')
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new MaritalStatus(value.marital_status_id, value.marital_status_name));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }

    SelectPayrollPeriods() {
        var connectionPool = this.context.connectionPool;

        return connectionPool.connect()
            .then(client => {
                return client.query('SELECT * FROM payroll_period')
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new PayrollPeriod(value.payroll_period_id, value.payroll_period_name));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }

    SelectTaxBrackets() {
        var connectionPool = this.context.connectionPool;

        return connectionPool.connect()
            .then(client => {
                return client.query('SELECT * FROM tax_bracket')
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new TaxBracket(value.tax_bracket_id, value.tax_bracket_rate));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }

    SelectTaxBracketById(taxBracketId) {
        var connectionPool = this.context.connectionPool;

        return connectionPool.connect()
            .then(client => {
                const query = {
                    text: 'SELECT * FROM tax_bracket WHERE tax_bracket_id = $1',
                    values: [taxBracketId],
                }

                return client.query(query)
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new TaxBracket(value.tax_bracket_id, value.tax_bracket_rate));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }

    SelectTaxYears() {
        var connectionPool = this.context.connectionPool;

        return connectionPool.connect()
            .then(client => {
                return client.query('SELECT * FROM tax_year')
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new TaxYear(value.tax_year_id, value.tax_year_value));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }

    SelectWithholdingAllowanceById(withholdingAllowanceId) {
        var connectionPool = this.context.connectionPool;

        return connectionPool.connect()
            .then(client => {

                const query = {
                    text: 'SELECT * FROM withholding_allowance WHERE withholding_allowance_id = $1',
                    values: [withholdingAllowanceId],
                }

                return client.query(query)
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new WithholdingAllowance(value.withholding_allowance_id, value.tax_year_id, value.payroll_period_id, value.withholding_allowance_amount));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }

    SelectWithholdingAllowances() {
        var connectionPool = this.context.connectionPool;

        return connectionPool.connect()
            .then(client => {
                return client.query('SELECT * FROM withholding_allowance')
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new WithholdingAllowance(value.withholding_allowance_id, value.tax_year_id, value.payroll_period_id, value.withholding_allowance_amount));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }

    SelectWithholdingSchedules() {
        var connectionPool = this.context.connectionPool;
        return connectionPool.connect()
            .then(client => {
                return client.query('SELECT * FROM withholding_schedule')
                    .then(resp => {
                        client.release();
                        return resp.rows.map(value => new WithholdingSchedule(value.withholding_scedule_id, value.tax_year_id, value.tax_bracket_id, value.payroll_period_id, value.marital_status_id, value.schedule_floor, value.base_tax_amount));
                    })
                    .catch(err => console.error(err.stack));
            })
            .catch(err => console.error(err.stack))
    }
}

module.exports = {
    taxDataService: taxDataService
};