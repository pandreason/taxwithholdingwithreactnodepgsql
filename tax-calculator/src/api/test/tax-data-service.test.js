const sinon = require('sinon');
const assert = require('chai').assert;
const { Pool, Client } = require('pg');
const { pgDatabaseContext } = require('../database-context.js');
const { taxDataService } = require('../tax-data-service.js');
const { MaritalStatus } = require('../models/marital-status.js');
const { PayrollPeriod } = require('../models/payroll-period.js');
const { TaxBracket } = require('../models/tax-bracket.js');
const { TaxYear } = require('../models/tax-year.js');
const { WithholdingAllowance } = require('../models/withholding-allowance.js');
const { WithholdingSchedule } = require('../models/withholding-schedule.js');

var Result = require('../../../node_modules/pg/lib/result.js')


const testConfig = {
    user: 'test',
    host: 'testhost',
    database: 'taxwithholding',
    password: 'admin',
    port: 5432
};

class PoolClient extends Client {
    release(err) { };
}

var testClient = new PoolClient();
var testResult = new Result();
var testResponseRow;

const connection = new Pool(testConfig, Client);

const context = new pgDatabaseContext(connection);
var dataService;

describe('tax-data-service - ', () => {
    describe('When selecting marital statuses ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { marital_status_id: 3, marital_status_name: "TestStatus" };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectMaritalStatuses();
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectMaritalStatuses();
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, 'SELECT * FROM marital_status');
                });

                it('the expected array of MaritalStatuses is returned', async () => {
                    var response = await dataService.SelectMaritalStatuses();
                    assert.deepEqual(response, [new MaritalStatus(testResponseRow.marital_status_id, testResponseRow.marital_status_name)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectMaritalStatuses();
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectMaritalStatuses();

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectMaritalStatuses();
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });

    describe('When selecting payroll periods ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { payroll_period_id: 3, payroll_period_name: "tri-annually" };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectPayrollPeriods();
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectPayrollPeriods();
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, 'SELECT * FROM payroll_period');
                });

                it('the expected array of Payrollperiods is returned', async () => {
                    var response = await dataService.SelectPayrollPeriods();
                    assert.deepEqual(response, [new PayrollPeriod(testResponseRow.payroll_period_id, testResponseRow.payroll_period_name)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectPayrollPeriods();
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectPayrollPeriods();

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectPayrollPeriods();
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });

    describe('When selecting tax brackets ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { payroll_period_id: 3, tax_bracket_rate: 0.66 };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectTaxBrackets();
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectTaxBrackets();
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, 'SELECT * FROM tax_bracket');
                });

                it('the expected array of tax bracket is returned', async () => {
                    var response = await dataService.SelectTaxBrackets();
                    assert.deepEqual(response, [new TaxBracket(testResponseRow.tax_bracket_id, testResponseRow.tax_bracket_rate)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectTaxBrackets();
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectTaxBrackets();

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectTaxBrackets();
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });

    describe('When selecting tax bracket by id ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { payroll_period_id: 3, tax_bracket_rate: 0.66 };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectTaxBracketById(3);
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectTaxBracketById(3);
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, {
                        text: 'SELECT * FROM tax_bracket WHERE tax_bracket_id = $1',
                        values: [3],
                    });
                });

                it('the expected array of tax bracket is returned', async () => {
                    var response = await dataService.SelectTaxBracketById(3);
                    assert.deepEqual(response, [new TaxBracket(testResponseRow.tax_bracket_id, testResponseRow.tax_bracket_rate)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectTaxBracketById(3);
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectTaxBracketById(3);

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectTaxBracketById(3);
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });

    describe('When selecting tax years ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { tax_year_id: 3, tax_year_value: 2016 };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectTaxYears();
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectTaxYears();
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, 'SELECT * FROM tax_year');
                });

                it('the expected array of tax year is returned', async () => {
                    var response = await dataService.SelectTaxYears();
                    assert.deepEqual(response, [new TaxYear(testResponseRow.tax_year_id, testResponseRow.tax_year_value)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectTaxYears();
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectTaxYears();

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectTaxYears();
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });

    describe('When selecting withholding allowances ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { withholding_allowance_id: 3, tax_year_id: 4, payroll_period_id: 5, withholding_allowance_amount: 42.42 };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectWithholdingAllowances();
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectWithholdingAllowances();
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, 'SELECT * FROM withholding_allowance');
                });

                it('the expected array of withholding allowances is returned', async () => {
                    var response = await dataService.SelectWithholdingAllowances();
                    assert.deepEqual(response, [new WithholdingAllowance(testResponseRow.withholding_allowance_id, testResponseRow.tax_year_id, testResponseRow.payroll_period_id, testResponseRow.withholding_allowance_amount)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectWithholdingAllowances();
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectWithholdingAllowances();

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectWithholdingAllowances();
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });

    describe('When selecting withholding allowance by id ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { withholding_allowance_id: 3, tax_year_id: 4, payroll_period_id: 5, withholding_allowance_amount: 42.42 };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectWithholdingAllowanceById(3);
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectWithholdingAllowanceById(3);
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, {
                        text: 'SELECT * FROM withholding_allowance WHERE withholding_allowance_id = $1',
                        values: [3],
                    });
                });

                it('the expected array of withholding allowance is returned', async () => {
                    var response = await dataService.SelectWithholdingAllowanceById(3);
                    assert.deepEqual(response, [new WithholdingAllowance(testResponseRow.withholding_allowance_id, testResponseRow.tax_year_id, testResponseRow.payroll_period_id, testResponseRow.withholding_allowance_amount)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectWithholdingAllowanceById(3);
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectWithholdingAllowanceById(3);

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectWithholdingAllowanceById(3);
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });

    describe('When selecting withholding schedules ', () => {
        describe('and the connection succeeds,', () => {
            beforeEach(() => {
                sandbox = sinon.createSandbox();
            });

            describe('and the query succeeds, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(testResult));
                    sandbox.stub(testClient, 'release');

                    testResponseRow = { withholding_scedule_id: 3, tax_year_id: 4, tax_bracket_id: 6, payroll_period_id: 5, marital_status_id: 7, schedule_floor: 0, base_tax_amount: 42.42 };

                    testResult.rows = [testResponseRow];

                    dataService = new taxDataService(context);
                });

                it('the pool is used to connect to the database', async () => {
                    await dataService.SelectWithholdingSchedules();
                    sandbox.assert.calledOnce(connection.connect);
                });

                it('the right query used to query the database', async () => {
                    await dataService.SelectWithholdingSchedules();
                    sandbox.assert.calledOnce(testClient.query);
                    sandbox.assert.calledWith(testClient.query, 'SELECT * FROM withholding_schedule');
                });

                it('the expected array of withholding schedules is returned', async () => {
                    var response = await dataService.SelectWithholdingSchedules();
                    assert.deepEqual(response, [new WithholdingSchedule(testResponseRow.withholding_scedule_id, testResponseRow.tax_year_id, testResponseRow.tax_bracket_id, testResponseRow.payroll_period_id, testResponseRow.marital_status_id, testResponseRow.schedule_floor, testResponseRow.base_tax_amount)]);
                });

                it('the client is released when the query completes', async () => {
                    await dataService.SelectWithholdingSchedules();
                    sandbox.assert.calledOnce(testClient.release);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                });
            });

            describe('and the query fails, then', () => {
                beforeEach(() => {
                    sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(testClient));
                    sandbox.stub(testClient, 'query').callsFake(() => Promise.resolve(new Error()));
                    sandbox.stub(testClient, 'release');
                    sandbox.stub(console, 'error');

                    dataService = new taxDataService(context);
                });

                it('the error is logged', async () => {
                    await dataService.SelectWithholdingSchedules();

                    sandbox.assert.calledOnce(console.error);
                });

                afterEach(() => {
                    connection.connect.restore();
                    testClient.query.restore();
                    testClient.release.restore();
                    console.error.restore();
                });
            });

            afterEach(() => {
                sandbox.reset();
            });
        });

        describe('and the connection fails, then', () => {
            beforeEach(() => {
                sandbox.stub(connection, "connect").callsFake(() => Promise.resolve(new Error()));
                sandbox.stub(console, 'error');

                dataService = new taxDataService(context);
            });

            it('the error is logged', async () => {
                try {
                    await dataService.SelectWithholdingSchedules();
                }
                catch(e) { }
                sandbox.assert.calledOnce(console.error);
            });

            afterEach(() => {
                connection.connect.restore();
                console.error.restore();
            });
        });
    });
});