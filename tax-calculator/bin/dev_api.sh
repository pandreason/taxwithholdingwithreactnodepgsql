#!/usr/bin/env bash

set -e

# Use nodemon to watch and reload our app codebase
nodemon --ignore src/www src/api/index.js
